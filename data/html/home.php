<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>PDT - PHP Development Tools</title>
</head>
<body>
<h1>
PDT - PHP Development Tools
</h1>
<div>
<iframe src="./" frameborder="0" width="100%" height="700"></iframe>

<hr>

<span><a href="./phpinfo.php">phpinfo</a></span>

</div>
<div>
&nbsp;
</div>
<div>
<?php $version = file_get_contents('../../version'); ?>
<?php $releases = file_get_contents('http://git.cxiangnet.cn/sunny5156/PDT/raw/master/version'); ?>
由<a href="http://pdt.cxiangnet.cn" target="_blank">PDT version:<?php echo $version;?></a>提供
</div>
<script>
var version = "<?php echo $version ?>";
var releases = "<?php echo $releases ?>";
//桌面提醒  
function notify(title, content) {  
          
        if(!title && !content){  
            title = "PDT新版本提醒";  
            content = "PDT有新的版本,请您更新!";  
        }  
        var iconUrl = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAQAAAD9CzEMAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfeBAoWAgUkKtiUAAADfElEQVRYw7WXaUhUURTHfzbmWpkTTGQupWlCBJZl02JEYFApZl8LEisILKL60oJBlBBkm0ILFAgpUV+iCKQgarLUVot2Bw21IalwPjgtrrcPM47z9vcaO+fLe7xz//97zz3/c++LwMjiySIXJ1kkkUQs0EsHb3nIE9wMGo7XtTR20sBXBhEK76eTekqw/yt4Osf4pAIsp2liO4lWwWMp470h+KgP4aKQSPPwKVxmwDS83/uoYaY5+AU0WwQf9UcsVsJFyN4LOM08RdQfPHjoxAdEks4ckrGpTK6D3dwOeY9msjRgDV8U83JzirWkEBecTCyz2cQNvCqr8FAURJtLLY2h8At5Jwv/wiFma6Qymnzq8CkoulgO2NlFGwIxNiCT57LQu+QZ7FcUGxWjBE1s4T7DgbeAJXJTEjRMDdNM15xciEMhzwDYqJTBnyXeXNEBcVTwU7O2ApsrzWWtBXj/BA9rKcdfbEPEkE1MIPwB5fRaIhAMUKDfMCZQyCsEgm6WWAKHDI7TxYheivyWzkl62GMJPIEdfNTVt2wdOXLtGdhRg/YxIhW8oIcBSwR28piq813YzCJp2AfuABnEaRGEiQ+AjaXUqzQNgQj3SB2zKNbToHK0DoKDhHEimUIpLUqCFlwUB0UWrjnYh1tyZiMQ/OQaTsXh86+WRRXfpAQCwTdOkDVOFBE4uc4vBL+RZMzNXhzjRBLDBlzcR648mim1qGZtS8CBau02sJ6ocSLR6CE+6nASrs51CASCH9SQ/T8JBIIOisODn2DwfZbF0p1KrlxPevN/w1YmWSI4SA9V0ruUxkHBZ46QYjEfqwMKfsm6sdyoE3wyvHQpLTWk1Xk5Q6oeQT8HLPamKVyVIHjJ198DH/uJNQ0fT7XkXjFMxWiS1C98fkVfItkU/HQuBu+ifq8bK45R8Dts4aliHc/YyEQD+GXck41qDJ2YQNBGOQnAKpX/Ax9XWEG0Bng2lfTIRrSSExrSxIUQMRXhUdkPLzfYRHpwT2zEk0YR5+lU0c6iUPgI7PRJzv4iqpmlMtdhPLTTziCQSDLJzFBZ1yu28cJoy5w8NuhQWu6SJkfbZlJDn0Xw35wjyRw8QCSFuBSFq+2v2Wz9iEpkG030G0CP8I4DJvWiYnZKqKNTlWYAD7co0//DN9NvIskkj5XMJw0H4OU73bhpoZU2/ugP/gueRxGV0JT/UgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNi0wOS0xN1QxNToxOTo1MSswODowMDoMIPQAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTQtMDQtMTBUMjI6MDI6MDUrMDg6MDArABRBAAAATXRFWHRzb2Z0d2FyZQBJbWFnZU1hZ2ljayA3LjAuMS02IFExNiB4ODZfNjQgMjAxNi0wOS0xNyBodHRwOi8vd3d3LmltYWdlbWFnaWNrLm9yZ93ZpU4AAAAYdEVYdFRodW1iOjpEb2N1bWVudDo6UGFnZXMAMaf/uy8AAAAZdEVYdFRodW1iOjpJbWFnZTo6SGVpZ2h0ADEwOTbq3GBaAAAAGHRFWHRUaHVtYjo6SW1hZ2U6OldpZHRoADExMDTBmwwRAAAAGXRFWHRUaHVtYjo6TWltZXR5cGUAaW1hZ2UvcG5nP7JWTgAAABd0RVh0VGh1bWI6Ok1UaW1lADEzOTcxMzg1MjWfr8r1AAAAEnRFWHRUaHVtYjo6U2l6ZQAzNi4xS0JzZGw0AAAAX3RFWHRUaHVtYjo6VVJJAGZpbGU6Ly8vaG9tZS93d3dyb290L3NpdGUvd3d3LmVhc3lpY29uLm5ldC9jZG4taW1nLmVhc3lpY29uLmNuL3NyYy8xMTQzOC8xMTQzODA3LnBuZzRaZGwAAAAASUVORK5CYII=";  
          
        if (window.webkitNotifications) {  
            //chrome老版本  
            if (window.webkitNotifications.checkPermission() == 0) {  
                var notif = window.webkitNotifications.createNotification(iconUrl, title, content);  
                notif.display = function() {}  
                notif.onerror = function() {}  
                notif.onclose = function() {}  
                notif.onclick = function() {this.cancel();}  
                notif.replaceId = 'Meteoric';  
                notif.show();  
            } else {  
                window.webkitNotifications.requestPermission($jy.notify);  
            }  
        }  
        else if("Notification" in window){  
            // 判断是否有权限  
            if (Notification.permission === "granted") {  
                var notification = new Notification(title, {  
                    "icon": iconUrl,  
                    "body": content,
					"tag":"PDTNotice",
					"renotify":true,
                });  
				
				notification.onclick = function(){
					notification.close();
					var href = "http://git.cxiangnet.cn/sunny5156/PDT/releases";
					window.open(href);
				}
            }  
            //如果没权限，则请求权限  
            else if (Notification.permission !== 'denied') {  
                Notification.requestPermission(function(permission) {  
                    // Whatever the user answers, we make sure we store the  
                    // information  
                    if (!('permission' in Notification)) {  
                        Notification.permission = permission;  
                    }  
                    //如果接受请求  
                    if (permission === "granted") {  
                        var notification = new Notification(title, {  
                            "icon": iconUrl,  
                            "body": content,
							"tag":"PDTNotice",
							"renotify":true,
                        });

						notification.onclick = function(){
							notification.close();
							var href = "http://git.cxiangnet.cn/sunny5156/PDT/releases";
							window.open(href);
						}

                    }  
                });  
            }  
        }  
    } 

if(parseInt(releases.replace('.')) > parseInt(version.replace('.')) ){
	notify();
}
</script>
<script src="http://stat.51unite.com/count.js"></script>
</body>
</html>