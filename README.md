
# PDT
#*PHP  Development Tools*

##**windows 下 php 集成开发环境工具**
##[官网](http://blog.cxiangnet.cn/pdt/)     
![加群](http://blog.cxiangnet.cn/wp-content/uploads/2016/12/group.png) 107882385

##涵盖
	nginx 1.12.0
	apache 2.4.3
	mysql 5.6.36
	redis 2.2.5
	mongodb
	ssdb 1.6.8.6
	php(默认7.1 nts vc14)
    php5.6(保留)
    
##操作介绍
	all_start.bat		启动所有服务
    all_stop.bat		关闭所有服务
    http_start.bat		启动apache
    http_stop.bat		关闭apache
    nginx_start.bat 	启动nginx
    nginx_stop.bat		关闭nginx
    phpcgi_start.bat 	启动php CGI
    phpcgi_stop.bat	 	关闭php CGI
    php5cgi_start.bat	启动php5.6 CGI
    php5cgi_stop.bat	关闭php5.6 CGI
    openresty_start.bat	启动openresty
    openresty_stop.bat	关闭openresty
    memcache_start.bat	启动memcache
    memcache_stop.bat	关闭memcache
    redis_start.bat		启动redis
    redis_stop.bat		关闭redis
    ssdb_start.bat		启动ssdb
    ssdb_stop.bat		关闭ssdb
    mongodb_start.bat	启动mongo
    mongodb_stop.bat	关闭mongo
    shell.bat			PDT目录cmd入口命令
    start_home.bat		打开PDT主页
    


##HTML文件夹说明
phpinfo.php  php信息查看
memcache.php memcache管理工具
adminer.php  mysql管理工具


#更新记录

##升级nginx mysql php
*	20170526 升级 nginx 1.12.0  mysql 5.6.36 php 7.1.5

##修改微信curl: (60) SSL certificate problem: unable to get local issuer certificate问题
*	20170220 根目录下创建cacert目录 安装相关证书

##添加PHP_CodeSniffer
*	20170124 添加 code sniffer 工具

##修复bug
20161122 PDT丢失 msvcr110.dll 错误解决方法
*	打开网址下载对应版本的exe执行安装
*	https://www.microsoft.com/zh-CN/download/details.aspx?id=30679



=======
# PDT
PHP  Development Tools

windows 下 php 开发环境工具

涵盖
nginx

apache

mysql 主从

redis

mongodb

ssdb

php7.1(默认)

php5.6

http://git.cxiangnet.cn/sunny5156/PDT
