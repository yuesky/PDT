特别说明:
  a.Apache Httpd是以Fastcgi的方式运行PHP.
  b.在运行某些旧的PHP程序时, Apache Httpd稳定性更好.
  c.支持Zend Guard Loader.
  d.Nginx默认监听80端口, Httpd默认监听8088端口
  e.PDT所使用组件全部来自官方下载链接, 除配置文件外未做任何改动

安装步骤:
  a.解压PDT_v2.2.0-vc9-nts-win32-x86.7z(解压到任何逻辑盘的根目录, 本例解压到d:\PDT目录)
  b.安装VC9运行时环境: 运行msvc9_x86_sp1.exe文件开始安装
  c.安装VC10运行时环境: 运行msvc10_x86.exe文件开始安装
  d.运行imagemagick.reg导入注册表(重要: 如果不是安装在d:\PDT目录,请自行修改此文件中的路径)
  e.运行d:\PDT\all_start.bat启动Nginx,Httpd,PHP,Memcache,MySQL,MongoDB和Cron服务
  f.运行d:\PDT\all_stop.bat停止上述服务
  g.可根据需要单独启动某项服务

1,System Requirements:
  380 MB free disk 
  128 MB RAM
  Windows XP/2003/2008/Vista/7/8/10
  Microsoft Visual C++ 2008 Redistributable Package (x86)

2,软件包:
  Nginx
  Apache Httpd
  PHP
  Memcached
  MySQL
  MongoDB
  Redis
  ssDB
  ImageMagick

3,可选: 打开\php\php.ini,编辑下面的内容(注意去掉前面的注释):
  include_path = ".;/PDT/php/pear"
  zend_extension="/PDT/php/ext/php_xdebug.dll"

4,Nginx配置:
  需要修改时,请编辑/PDT/nginx/conf/nginx.conf
  Nginx默认站点目录及对应访问URL:
  /PDT/html => http://localhost/
  注意:Nginx默认监听80端口

5,Apache Httpd配置:
  需要修改时,请编辑/PDT/httpd/conf/httpd.conf
  Httpd默认站点目录及对应访问URL:
  /PDT/html => http://localhost:8088/
  注意:Httpd默认监听8088端口

6,MySQL密码：
   用户: root
   密码: 
   (means no password!)
  
7,MongoDB密码:
   用户: 
   密码: 

