@echo off

SET PATH=%SystemRoot%

SET tmp=%~dp0temp

REM Memcached (m = memory to use in mb, c = max connections allowed)
bin\runhiddenconsole memcached\memcached.exe -m 16 -c 1024

exit