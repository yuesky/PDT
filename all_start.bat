@echo off

set tmp=%~dp0temp

set PATH=%~dp0;%~dp0bin;%~dp0php;%~dp0mysql\bin;%~dp0mongodb\bin;%~dp0sendmail
set PATH=%~dp0imagemagick;%SystemRoot%;%PATH%
set PATH=%Path%;%~dp0httpd\bin

Rem On Windows the following is not valid  
Rem set php_fcgi_children = 20

Rem Each process handles the maximum number of requests
Rem set php_fcgi_max_requests = 2000

Rem Start php-fastcgi on port 9000
set PHP_HELP_MAX_REQUESTS = 100
for /l %%i in (1 1 1) do (
REM bin\runhiddenconsole php\php-cgi.exe -b 127.0.0.1:9000 -c php\php.ini
bin\runhiddenconsole php_cgi_spawner\php-cgi-spawner.exe "php\php-cgi.exe -c php\php.ini" 9000 4+16
)

Rem Start Memcached (m = memory to use in mb, c = max connections allowed)
bin\runhiddenconsole memcached\memcached.exe -m 10 -c 2048

Rem Start MySQL using the mysql\my.ini config file
start mysql\bin\mysqld.exe --defaults-extra-file=\PDT\mysql\my.ini --datadir=\PDT\data\mysql

Rem MongoDB
del \PDT\data\mongodb\mongod.lock
bin\runhiddenconsole mongodb\bin\mongod.exe --dbpath=\PDT\data\mongodb

Rem Nginx
cd nginx
start nginx.exe
cd ..

Rem Apache2
bin\runhiddenconsole httpd\bin\httpd.exe

Rem Cron
cd cron
..\bin\runhiddenconsole cron.exe
cd ..


Rem Redis
Rem cd redis
bin\runhiddenconsole redis\redis-server.exe  redis\redis.conf
cd ..

Rem FileZillaFTP
Rem filezillaftp\filezillaserver.exe
Rem filezillaftp\filezillaserver.exe /start

Rem start home page
start  http://localhost/home.php

exit