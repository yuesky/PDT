@echo off

taskkill /f /im php-cgi-spawner.exe
taskkill /f /im php-cgi.exe
taskkill /f /im nginx.exe
taskkill /f /im httpd.exe
taskkill /f /im memcached.exe
taskkill /f /im mysqld.exe
taskkill /f /im mongod.exe
taskkill /f /im cron.exe
taskkill /f /im redis-server.exe


Rem taskkill /f /im filezillaserver.exe

del httpd\logs\httpd.pid

pause
rem exit
