@echo off

SET PATH=%SystemRoot%

SET PATH=%Path%;%~dp0httpd\bin
SET PATH=%Path%;%~dp0imagemagick
SET PATH=%Path%;%~dp0php

SET tmp=%~dp0temp

Rem Apache httpd 2.2
bin\runhiddenconsole httpd\bin\httpd.exe
