@echo off

SET PATH=%SystemRoot%

SET PATH=%Path%;%~dp0mysql\bin

SET tmp=%~dp0temp

REM Start MySQL using the mysql\my.ini config file
REM  --lower_case_file_system=no 
start mysql\bin\mysqld.exe --defaults-extra-file=\PDT\mysql\my.ini --datadir=\PDT\data\mysql  --port=3306

exit